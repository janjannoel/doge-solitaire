import React, { FunctionComponent } from 'react';

import Solitaire from 'components/solitaire/Solitaire';

const MainPage: FunctionComponent = () => <Solitaire />;

export default MainPage;
