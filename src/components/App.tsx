import React, { FunctionComponent } from 'react';
import { Provider } from 'react-redux';

import store from 'store';

import MainPage from './pages/main/MainPage';

const App: FunctionComponent = () => (
  <Provider store={store}>
    <MainPage />
  </Provider>
);

export default App;
