import React, { FunctionComponent } from 'react';
import { connect, StateMapper } from 'react-redux';

import Card from 'components/solitaire/card/Card';
import { AppState } from 'store/reducer';
import { Pile } from 'models/Pile';
import { getTalonPile } from 'store/game/selectors';

import styles from './TalonPile.module.css';

type StateProps = {
  talonPile: Pile;
};

type Props = StateProps;

const TalonPile: FunctionComponent<Props> = ({ talonPile }) => (
  <div className={styles.wrapper}>
    {talonPile.length > 0 ? (
      talonPile.map(card => <Card key={card.id} card={card} className={styles.card} />)
    ) : (
      <div className={styles.emptyPile} />
    )}
  </div>
);

const mapStateToProps: StateMapper<StateProps, AppState> = state => ({
  talonPile: getTalonPile(state)
});

export default connect(mapStateToProps)(TalonPile);
