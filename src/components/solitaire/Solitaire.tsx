import React, { FunctionComponent, useEffect, useCallback } from 'react';
import { DispatchMapper, connect } from 'react-redux';
import { DragDropContext, DropResult } from 'react-beautiful-dnd';

import { Pile } from 'models/Pile';
import { Location } from 'models/Location';
import { startGame as doStartGame, tryMoveCard as doTryMoveCard } from 'store/game/actions';
import generateShuffledDeck from 'helpers/cards/generateShuffledDeck';
import convertDroppableIdToLocation from 'helpers/cards/convertDroppableIdToLocation';
import convertDraggableIdToCardId from 'helpers/cards/convertDraggableIdToCardId';

// import StockPile from './stock/StockPile';
// import TalonPile from './talon/TalonPile';
import Foundations from './foundations/Foundations';
import Tableau from './tableau/Tableau';
import styles from './Solitaire.module.css';

type DispatchProps = {
  startGame: (pile: Pile) => void;
  moveCard: (card: number, source: Location, destination: Location) => void;
};

type Props = DispatchProps;

const Solitaire: FunctionComponent<Props> = ({ startGame, moveCard }) => {
  useEffect(() => {
    startGame(generateShuffledDeck());
  }, [startGame]);

  const handleDragEnd = useCallback(
    ({ draggableId, source, destination }: DropResult) => {
      const cardId = convertDraggableIdToCardId(draggableId);
      const moveFrom = convertDroppableIdToLocation(source.droppableId);
      const moveTo = destination && convertDroppableIdToLocation(destination.droppableId);

      if (moveTo) {
        console.log(cardId, moveFrom, moveTo);
        moveCard(cardId, moveFrom, moveTo);
      }
    },
    [moveCard]
  );

  return (
    <DragDropContext onDragEnd={handleDragEnd}>
      <div className={styles.container}>
        {/* <StockPile /> */}
        {/* <TalonPile /> */}
        <Foundations />
        <Tableau />
      </div>
    </DragDropContext>
  );
};

const mapDispatchToProps: DispatchMapper<DispatchProps, {}> = dispatch => ({
  startGame(cards) {
    dispatch(doStartGame(cards));
  },
  moveCard(card, source, destination) {
    dispatch(doTryMoveCard(card, source, destination));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(Solitaire);
