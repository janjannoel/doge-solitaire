import React, { FunctionComponent } from 'react';
import { connect, StateMapper } from 'react-redux';

import Card from 'components/solitaire/card/Card';
import { AppState } from 'store/reducer';
import { Pile } from 'models/Pile';
import { getStockPile } from 'store/game/selectors';

import styles from './StockPile.module.css';

type StateProps = {
  stockPile: Pile;
};

type Props = StateProps;

const StockPile: FunctionComponent<Props> = ({ stockPile }) => (
  <div className={styles.wrapper}>
    {stockPile.length > 0 ? (
      stockPile.map(card => <Card key={card.id} card={card} className={styles.card} />)
    ) : (
      <div className={styles.emptyPile} />
    )}
  </div>
);

const mapStateToProps: StateMapper<StateProps, AppState> = state => ({
  stockPile: getStockPile(state)
});

export default connect(mapStateToProps)(StockPile);
