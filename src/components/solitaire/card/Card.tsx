import React, { FunctionComponent } from 'react';
import classnames from 'classnames';

import { Card as CardModel } from 'models/Card';
import { Color } from 'models/Color';
import noop from 'helpers/noop';

import styles from './Card.module.css';

type OwnProps = {
  card: CardModel;
  className?: string;
  onClick?: (card: CardModel) => void;
  onDoubleClick?: (card: CardModel) => void;
};

type Props = OwnProps;

const Card: FunctionComponent<Props> = ({ card, className, onClick = noop, onDoubleClick = noop }) => (
  <div
    role="button"
    tabIndex={0}
    className={classnames(className, styles.container)}
    onClick={() => onClick(card)}
    onDoubleClick={() => onDoubleClick(card)}
    onKeyPress={noop}
  >
    <div className={classnames(styles.card, card.isFacedDown ? styles.back : styles.front)}>
      {!card.isFacedDown && (
        <>
          <div className={classnames(styles.topLabel, card.color === Color.Black ? styles.black : styles.red)}>
            <div>{card.rank}</div>
            <div>{card.suit}</div>
          </div>
          <div className={styles.content} />
          <div className={classnames(styles.bottomLabel, card.color === Color.Black ? styles.black : styles.red)}>
            <div>{card.rank}</div>
            <div>{card.suit}</div>
          </div>
        </>
      )}
    </div>
  </div>
);

export default Card;
