import React, { FunctionComponent, useCallback } from 'react';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import { connect, DispatchMapper } from 'react-redux';

import { Card as CardModel } from 'models/Card';
import Card from 'components/solitaire/card/Card';
import { PileType } from 'models/PileType';
import { Location } from 'models/Location';
import { tryMoveCard as doTryMoveCard } from 'store/game/actions';

import styles from './DraggableCards.module.css';

type OwnProps = {
  cards: Array<CardModel>;
  column: string;
  index: number;
};

type DispatchProps = {
  moveCard: (card: number, source: Location, destination: Location) => void;
};

type Props = OwnProps & DispatchProps;

const DraggableCards: FunctionComponent<Props> = ({ index, cards, column, moveCard }) => {
  const [firstCard, ...restOfCards] = cards;

  const handleLastCardDoubleClick = useCallback(
    (card: CardModel) => {
      moveCard(
        card.id,
        { pileType: PileType.Tableau, position: column, index },
        { pileType: PileType.Foundation, position: card.suit, index: null }
      );
    },
    [column, index, moveCard]
  );

  return (
    <Droppable droppableId={`${PileType.Tableau}-${column}-${index}`} isDropDisabled>
      {dropProvided => (
        <div className={styles.container} {...dropProvided.droppableProps} ref={dropProvided.innerRef}>
          <Draggable draggableId={`card-${firstCard.id}`} index={index} key={firstCard.id}>
            {dragProvided => (
              <div {...dragProvided.draggableProps} {...dragProvided.dragHandleProps} ref={dragProvided.innerRef}>
                <Card
                  card={firstCard}
                  {...(restOfCards.length > 0
                    ? { className: styles.overlappedCard }
                    : { onDoubleClick: handleLastCardDoubleClick })}
                />

                {restOfCards.length > 0 && (
                  <DraggableCards column={column} index={index + 1} cards={restOfCards} moveCard={moveCard} />
                )}
                {dropProvided.placeholder}
              </div>
            )}
          </Draggable>
        </div>
      )}
    </Droppable>
  );
};

const mapDispatchToProps: DispatchMapper<DispatchProps, {}> = dispatch => ({
  moveCard(card, source, destination) {
    dispatch(doTryMoveCard(card, source, destination));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(DraggableCards);
