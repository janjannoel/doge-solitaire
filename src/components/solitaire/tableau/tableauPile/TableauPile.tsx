import React, { FunctionComponent, useCallback } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import { connect, DispatchMapper } from 'react-redux';
import classnames from 'classnames';

import { Pile } from 'models/Pile';
import { PileType } from 'models/PileType';
import { Card as CardModel } from 'models/Card';
import Card from 'components/solitaire/card/Card';
import { revealTableauCard as doRevealTableauCard } from 'store/game/actions';

import DraggableCards from '../draggableCards/DraggableCards';

import styles from './TableauPile.module.css';

type OwnProps = {
  pile: Pile;
  column: string;
};

type DispatchProps = {
  revealCard: (card: CardModel, column: string) => void;
};

type Props = OwnProps & DispatchProps;

const TableauPile: FunctionComponent<Props> = ({ pile, column, revealCard }) => {
  let firstFacedUpCardIndex = pile.findIndex(card => !card.isFacedDown);
  firstFacedUpCardIndex = firstFacedUpCardIndex === -1 ? pile.length : firstFacedUpCardIndex;
  const [lastCard] = pile.slice(-1);
  const facedDownCards = pile.slice(0, firstFacedUpCardIndex);
  const facedUpCards = pile.slice(firstFacedUpCardIndex);

  const handleFacedDownCardClick = useCallback(
    (card: CardModel) => {
      revealCard(card, column);
    },
    [column, revealCard]
  );

  return (
    <div className={styles.container}>
      {facedDownCards.map(card => (
        <Card
          key={card.id}
          card={card}
          {...(card === lastCard
            ? { className: styles.flippable, onClick: handleFacedDownCardClick }
            : { className: styles.overlappedCard })}
        />
      ))}
      {facedUpCards.length > 0 && <DraggableCards column={column} index={firstFacedUpCardIndex} cards={facedUpCards} />}
      <Droppable droppableId={`${PileType.Tableau}-${column}`}>
        {({ droppableProps, innerRef, placeholder }) => (
          <div
            className={classnames(styles.dropArea, { [styles.isEmpty]: pile.length === 0 })}
            {...droppableProps}
            ref={innerRef}
          >
            {placeholder}
          </div>
        )}
      </Droppable>
    </div>
  );
};

const mapDispatchToProps: DispatchMapper<DispatchProps, {}> = dispatch => ({
  revealCard(card, column) {
    dispatch(doRevealTableauCard(card, column));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(TableauPile);
