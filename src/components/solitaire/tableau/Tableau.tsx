import React, { FunctionComponent } from 'react';
import { connect, StateMapper } from 'react-redux';

import { Pile } from 'models/Pile';
import { AppState } from 'store/reducer';
import { getTableauPileList } from 'store/game/selectors';

import TableauPile from './tableauPile/TableauPile';
import styles from './Tableau.module.css';

type StateProps = {
  tableauPileList: Array<{ column: string; pile: Pile }>;
};

type Props = StateProps;

const Tableau: FunctionComponent<Props> = ({ tableauPileList }) => (
  <div className={styles.container}>
    {tableauPileList.map(({ column, pile }) => (
      <TableauPile key={column} column={column} pile={pile} />
    ))}
  </div>
);

const mapStateToProps: StateMapper<StateProps, AppState> = state => ({
  tableauPileList: getTableauPileList(state)
});

export default connect(mapStateToProps)(Tableau);
