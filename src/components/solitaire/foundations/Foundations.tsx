import React, { FunctionComponent } from 'react';
import { connect, StateMapper } from 'react-redux';

import { Pile } from 'models/Pile';
import { AppState } from 'store/reducer';
import {
  getClubFoundationsPile,
  getSpadeFoundationsPile,
  getHeartFoundationsPile,
  getDiamondFoundationsPile
} from 'store/game/selectors';
import { Suit } from 'models/Suit';

import FoundationPile from './foundationPile/FoundationPile';
import styles from './Foundations.module.css';

type StateProps = {
  clubPile: Pile;
  spadePile: Pile;
  heartPile: Pile;
  diamondPile: Pile;
};

type Props = StateProps;

const Foundations: FunctionComponent<Props> = ({ clubPile, spadePile, heartPile, diamondPile }) => (
  <div className={styles.container}>
    <FoundationPile suit={Suit.Club} pile={clubPile} />
    <FoundationPile suit={Suit.Spade} pile={spadePile} />
    <FoundationPile suit={Suit.Heart} pile={heartPile} />
    <FoundationPile suit={Suit.Diamond} pile={diamondPile} />
  </div>
);

const mapStateToProps: StateMapper<StateProps, AppState> = state => ({
  clubPile: getClubFoundationsPile(state),
  spadePile: getSpadeFoundationsPile(state),
  heartPile: getHeartFoundationsPile(state),
  diamondPile: getDiamondFoundationsPile(state)
});

export default connect(mapStateToProps)(Foundations);
