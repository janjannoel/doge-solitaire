import React, { FunctionComponent } from 'react';
import { Droppable, Draggable } from 'react-beautiful-dnd';

import { Suit } from 'models/Suit';
import { Pile } from 'models/Pile';
import Card from 'components/solitaire/card/Card';
import { PileType } from 'models/PileType';

import styles from './FoundationPile.module.css';

type OwnProps = {
  suit: Suit;
  pile: Pile;
};

type Props = OwnProps;

const FoundationPile: FunctionComponent<Props> = ({ suit, pile }) => {
  const [topCard, secondTopCard] = pile.slice(-2).reverse();

  return (
    <div className={styles.container}>
      {secondTopCard ? (
        <Card key={secondTopCard.id} card={secondTopCard} />
      ) : (
        <div className={styles.emptyPile}>{suit}</div>
      )}

      {topCard && (
        <div className={styles.topCardWrapper}>
          <Droppable droppableId={`${PileType.Foundation}-${suit}-${pile.length}`} isDropDisabled>
            {dropProvided => (
              <div {...dropProvided.droppableProps} ref={dropProvided.innerRef}>
                <Draggable draggableId={`card-${topCard.id}`} index={pile.length} key={topCard.id}>
                  {dragProvided => (
                    <div {...dragProvided.draggableProps} {...dragProvided.dragHandleProps} ref={dragProvided.innerRef}>
                      <Card card={topCard} />
                      {dropProvided.placeholder}
                    </div>
                  )}
                </Draggable>
              </div>
            )}
          </Droppable>
        </div>
      )}

      <Droppable droppableId={`${PileType.Foundation}-${suit}`}>
        {({ droppableProps, innerRef, placeholder }) => (
          <div className={styles.dropArea} {...droppableProps} ref={innerRef}>
            {placeholder}
          </div>
        )}
      </Droppable>
    </div>
  );
};

export default FoundationPile;
