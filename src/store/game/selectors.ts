import { createSelector } from 'reselect';

import { AppState } from 'store/reducer';
import { Suit } from 'models/Suit';

export const getTableauPileMap = (state: AppState) => state.game.tableau;

export const getTableauPileList = createSelector(
  getTableauPileMap,
  tableauMap =>
    Object.keys(tableauMap).map(column => ({
      column,
      pile: tableauMap[column]
    }))
);

export const getFoundationsPileMap = (state: AppState) => state.game.foundations;

export const getClubFoundationsPile = (state: AppState) => getFoundationsPileMap(state)[Suit.Club];

export const getSpadeFoundationsPile = (state: AppState) => getFoundationsPileMap(state)[Suit.Spade];

export const getHeartFoundationsPile = (state: AppState) => getFoundationsPileMap(state)[Suit.Heart];

export const getDiamondFoundationsPile = (state: AppState) => getFoundationsPileMap(state)[Suit.Diamond];

export const getStockPile = (state: AppState) => state.game.stock;

export const getTalonPile = (state: AppState) => state.game.talon;
