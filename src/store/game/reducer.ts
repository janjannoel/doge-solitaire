import { Suit } from 'models/Suit';
import { Pile } from 'models/Pile';
import { Card } from 'models/Card';

import {
  GameState,
  GameAction,
  START_GAME,
  REVEAL_TABLEAU_CARD,
  MOVE_TABLEAU_CARD_TO_TABLEAU,
  MOVE_TABLEAU_CARD_TO_FOUNDATION
} from './types';

// Default State
export const DEFAULT_STATE: GameState = {
  tableau: {
    '0': [],
    '1': [],
    '2': [],
    '3': [],
    '4': [],
    '5': [],
    '6': []
  },
  foundations: {
    [Suit.Club]: [],
    [Suit.Spade]: [],
    [Suit.Heart]: [],
    [Suit.Diamond]: []
  },
  stock: [],
  talon: []
};

// Reducer
function startGame(pile: Pile) {
  const state: GameState = {
    tableau: {
      '0': pile.slice(0, 1),
      '1': pile.slice(1, 3),
      '2': pile.slice(3, 6),
      '3': pile.slice(6, 10),
      '4': pile.slice(10, 15),
      '5': pile.slice(15, 21),
      '6': pile.slice(21, 28)
    },
    foundations: {
      [Suit.Club]: [],
      [Suit.Spade]: [],
      [Suit.Heart]: [],
      [Suit.Diamond]: []
    },
    stock: pile.slice(28),
    talon: []
  };

  // Reveal the bottom-most card for each tableau pile
  Object.values(state.tableau).forEach(tableauPile => {
    const lastCard = tableauPile.slice(-1)[0];
    lastCard.isFacedDown = false;
  });

  return state;
}

function moveTableauCardToTableau(state: GameState, { card, from, to }: { card: Card; from: string; to: string }) {
  const sourcePile = state.tableau[from];
  const destinationPile = state.tableau[to];
  const cardsTransferred = sourcePile.slice(sourcePile.findIndex(x => x === card));

  return {
    ...state,
    tableau: {
      ...state.tableau,
      [from]: sourcePile.filter(x => !cardsTransferred.includes(x)),
      [to]: destinationPile.concat(cardsTransferred)
    }
  };
}

function moveTableauCardToFoundation(
  state: GameState,
  { card, from, to }: { card: Card; from: string; to: Exclude<Suit, Suit.Joker> }
) {
  return {
    ...state,
    tableau: {
      ...state.tableau,
      [from]: state.tableau[from].filter(x => x !== card)
    },
    foundations: {
      ...state.foundations,
      [to]: state.foundations[to].concat([card])
    }
  };
}

function revealTableauCard(state: GameState, { card, column }: { card: Card; column: string }) {
  return {
    ...state,
    tableau: {
      ...state.tableau,
      [column]: state.tableau[column].map(x => {
        if (x === card) {
          return { ...x, isFacedDown: false };
        }

        return x;
      })
    }
  };
}

const reducer = (state = DEFAULT_STATE, action: GameAction) => {
  switch (action.type) {
    case START_GAME:
      return startGame(action.payload);
    case MOVE_TABLEAU_CARD_TO_TABLEAU:
      return moveTableauCardToTableau(state, action.payload);
    case MOVE_TABLEAU_CARD_TO_FOUNDATION:
      return moveTableauCardToFoundation(state, action.payload);
    case REVEAL_TABLEAU_CARD:
      return revealTableauCard(state, action.payload);
    default:
      return state;
  }
};

export default reducer;
