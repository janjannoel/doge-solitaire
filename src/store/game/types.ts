import { Pile } from 'models/Pile';
import { Suit } from 'models/Suit';
import { Card } from 'models/Card';

// State
export type GameState = {
  readonly tableau: Record<string, Pile>;
  readonly foundations: Record<Exclude<Suit, Suit.Joker>, Pile>;
  readonly stock: Pile;
  readonly talon: Pile;
};

// Actions
export const START_GAME = '@@game/START_GAME';
export const MOVE_TABLEAU_CARD_TO_TABLEAU = '@@game/MOVE_TABLEAU_CARD_TO_TABLEAU';
export const MOVE_TABLEAU_CARD_TO_FOUNDATION = '@@game/MOVE_TABLEAU_CARD_TO_FOUNDATION';
export const REVEAL_TABLEAU_CARD = '@@game/REVEAL_TABLEAU_CARD';

interface StartGameAction {
  type: typeof START_GAME;
  payload: Pile;
}

interface MoveTableauCardToTableauAction {
  type: typeof MOVE_TABLEAU_CARD_TO_TABLEAU;
  payload: {
    card: Card;
    from: string;
    to: string;
  };
}

interface MoveTableauCardToFoundationAction {
  type: typeof MOVE_TABLEAU_CARD_TO_FOUNDATION;
  payload: {
    card: Card;
    from: string;
    to: Exclude<Suit, Suit.Joker>;
  };
}

interface RevealTableauCardAction {
  type: typeof REVEAL_TABLEAU_CARD;
  payload: {
    card: Card;
    column: string;
  };
}

export type GameAction = Readonly<
  StartGameAction | MoveTableauCardToTableauAction | MoveTableauCardToFoundationAction | RevealTableauCardAction
>;
