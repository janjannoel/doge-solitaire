import { ThunkAction } from 'redux-thunk';

import { Pile } from 'models/Pile';
import { Location } from 'models/Location';
import { PileType } from 'models/PileType';
import { AppState } from 'store/reducer';
import validateMoveCardToTableauPile from 'helpers/cards/validations/validateMoveCardToTableauPile';
import { Card } from 'models/Card';
import { Suit } from 'models/Suit';
import validateMoveCardToFoundationPile from 'helpers/cards/validations/validateMoveCardToFoundationPile';

import {
  GameAction,
  START_GAME,
  REVEAL_TABLEAU_CARD,
  MOVE_TABLEAU_CARD_TO_TABLEAU,
  MOVE_TABLEAU_CARD_TO_FOUNDATION
} from './types';
import { getTableauPileMap, getFoundationsPileMap } from './selectors';

// Actions
export function startGame(pile: Pile): GameAction {
  return {
    type: START_GAME,
    payload: pile
  };
}

export function moveTableauCardToTableau(card: Card, from: string, to: string): GameAction {
  return {
    type: MOVE_TABLEAU_CARD_TO_TABLEAU,
    payload: { card, from, to }
  };
}

export function moveTableauCardToFoundation(card: Card, from: string, to: Exclude<Suit, Suit.Joker>): GameAction {
  return {
    type: MOVE_TABLEAU_CARD_TO_FOUNDATION,
    payload: { card, from, to }
  };
}

export function revealTableauCard(card: Card, column: string): GameAction {
  return {
    type: REVEAL_TABLEAU_CARD,
    payload: { card, column }
  };
}

// Thunk Actions
export function tryMoveCard(
  cardId: number,
  source: Location,
  destination: Location
): ThunkAction<void, AppState, never, GameAction> {
  return (dispatch, getState) => {
    const state = getState();

    if (destination.pileType === PileType.Tableau) {
      // Move card from Tableau to Tableau
      if (source.pileType === PileType.Tableau) {
        const card = getTableauPileMap(state)[source.position].find(match => match.id === cardId);
        if (card && validateMoveCardToTableauPile(card, getTableauPileMap(state)[destination.position])) {
          dispatch(moveTableauCardToTableau(card, source.position, destination.position));
        }
      }
    }

    if (destination.pileType === PileType.Foundation && destination.position !== Suit.Joker) {
      // Move card from Tableau to Foundation
      if (source.pileType === PileType.Tableau) {
        const card = getTableauPileMap(state)[source.position].find(match => match.id === cardId);
        const [lastCard] = getTableauPileMap(state)[source.position].slice(-1);
        if (
          card &&
          card === lastCard &&
          validateMoveCardToFoundationPile(
            card,
            destination.position,
            getFoundationsPileMap(state)[destination.position]
          )
        ) {
          dispatch(moveTableauCardToFoundation(card, source.position, destination.position));
        }
      }
    }
  };
}
