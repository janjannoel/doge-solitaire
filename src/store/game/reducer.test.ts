import { Pile } from 'models/Pile';
import { Suit } from 'models/Suit';
import convertNumberToCard from 'helpers/cards/convertNumberToCard';

import reducer, { DEFAULT_STATE } from './reducer';
import { startGame } from './actions';

// Utility Functions
function createPile(): Pile {
  return Array.from({ length: 52 }, (_, index) => convertNumberToCard(index));
}

// Tests
test('START_GAME', () => {
  const newState = reducer(DEFAULT_STATE, startGame(createPile()));
  expect(newState).toMatchSnapshot();
  expect(newState.tableau['0'].length).toEqual(1);
  expect(newState.tableau['1'].length).toEqual(2);
  expect(newState.tableau['2'].length).toEqual(3);
  expect(newState.tableau['3'].length).toEqual(4);
  expect(newState.tableau['4'].length).toEqual(5);
  expect(newState.tableau['5'].length).toEqual(6);
  expect(newState.tableau['6'].length).toEqual(7);
  expect(newState.stock.length).toEqual(24);
  expect(newState.talon.length).toEqual(0);
  expect(newState.foundations[Suit.Club].length).toEqual(0);
  expect(newState.foundations[Suit.Spade].length).toEqual(0);
  expect(newState.foundations[Suit.Heart].length).toEqual(0);
  expect(newState.foundations[Suit.Diamond].length).toEqual(0);
});
