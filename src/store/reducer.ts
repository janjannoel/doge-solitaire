import { combineReducers } from 'redux';

import game from './game/reducer';

// Reducer
const reducer = combineReducers({
  game
});

// State
export type AppState = ReturnType<typeof reducer>;

export default reducer;
