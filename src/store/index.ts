import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducer from './reducer';

const composeEnhancer = composeWithDevTools({});

const store = createStore(reducer, composeEnhancer(applyMiddleware(thunk)));

export default store;
