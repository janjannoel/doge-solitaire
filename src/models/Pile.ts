import { Card } from './Card';

export type Pile = ReadonlyArray<Card>;
