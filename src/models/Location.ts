import { PileType } from './PileType';
import { Suit } from './Suit';

interface BaseLocation<T extends PileType> {
  readonly pileType: T;
  readonly position: T extends PileType.Foundation ? Suit : T extends PileType.Tableau ? string : null;
  readonly index: T extends PileType.Tableau ? number | null : null;
}

export type Location =
  | BaseLocation<PileType.Foundation>
  | BaseLocation<PileType.Tableau>
  | BaseLocation<PileType.Stock>
  | BaseLocation<PileType.Talon>;
