export enum Suit {
  Joker = '🃏',
  Club = '♣',
  Spade = '♠',
  Heart = '♥️',
  Diamond = '♦'
}
