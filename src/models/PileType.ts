export enum PileType {
  Tableau = 'TABLEAU',
  Foundation = 'FOUNDATION',
  Stock = 'STOCK',
  Talon = 'TALON'
}
