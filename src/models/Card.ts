import { Suit } from './Suit';
import { Rank } from './Rank';
import { Color } from './Color';

interface BaseCard<T extends Suit> {
  readonly id: number;
  readonly suit: T;
  readonly rank: T extends Suit.Joker ? Rank.Joker : Exclude<Rank, Rank.Joker>;
  readonly color: T extends Suit.Club | Suit.Spade
    ? Color.Black
    : T extends Suit.Heart | Suit.Diamond
    ? Color.Red
    : T extends Suit.Joker
    ? Color
    : never;
  isFacedDown: boolean;
}

export type Card =
  | BaseCard<Suit.Joker>
  | BaseCard<Suit.Club>
  | BaseCard<Suit.Spade>
  | BaseCard<Suit.Heart>
  | BaseCard<Suit.Diamond>;
