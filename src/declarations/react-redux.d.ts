import 'react-redux';
import { Dispatch } from 'redux';

interface EnhancedMapStateToProps<StateProps, AppState, OwnProps> {
  (state: AppState, ownProps: OwnProps): StateProps;
}

interface EnhancedMapStateToPropsFactory<StateProps, AppState, OwnProps> {
  (state: AppState, ownProps: OwnProps): EnhancedMapStateToProps<StateProps, AppState, OwnProps>;
}

interface EnhancedDispatch extends Dispatch {
  <Action>(action: Action): Action extends Function ? ReturnType<Action> : Action;
}

interface EnhancedMapDispatchToProps<DispatchProps, OwnProps> {
  (dispatch: EnhancedDispatch, ownProps: OwnProps): DispatchProps;
}

interface EnhancedMapDispatchToPropsFactory<DispatchProps, OwnProps> {
  (dispatch: EnhancedDispatch, ownProps: OwnProps): EnhancedMapDispatchToProps<DispatchProps, OwnProps>;
}

declare module 'react-redux' {
  type StateMapper<StateProps, AppState, OwnProps = {}> =
    | EnhancedMapStateToProps<StateProps, AppState, OwnProps>
    | EnhancedMapStateToPropsFactory<StateProps, AppState, OwnProps>;

  type DispatchMapper<DispatchProps, OwnProps = {}> =
    | DispatchProps
    | EnhancedMapDispatchToProps<DispatchProps, OwnProps>
    | EnhancedMapDispatchToPropsFactory<DispatchProps, OwnProps>;
}
