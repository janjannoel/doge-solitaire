import { Card } from 'models/Card';
import { Pile } from 'models/Pile';
import { Rank } from 'models/Rank';

import convertRankToValue from '../convertRankToValue';

export default function validateMoveCardToTableauPile(card: Card, tableauPile: Pile) {
  // Card moved is faced down
  if (card.isFacedDown) {
    return false;
  }

  // Only King can be moved to an empty tableau pile
  if (tableauPile.length === 0) {
    if (card.rank === Rank.King) {
      return true;
    }
    return false;
  }

  const [lastTableauPileCard] = tableauPile.slice(-1);

  // Destination is faced down
  if (lastTableauPileCard.isFacedDown) {
    return false;
  }

  // Destination and card moved should be consecutive and has different color
  if (
    lastTableauPileCard.color !== card.color &&
    convertRankToValue(lastTableauPileCard.rank) - convertRankToValue(card.rank) === 1
  ) {
    return true;
  }

  return false;
}
