import { Card } from 'models/Card';
import { Pile } from 'models/Pile';
import { Rank } from 'models/Rank';
import { Suit } from 'models/Suit';

import convertRankToValue from '../convertRankToValue';

export default function validateMoveCardToFoundationPile(card: Card, suit: Suit, foundationPile: Pile) {
  // Card moved is faced down
  if (card.isFacedDown) {
    return false;
  }

  // Card moved doesn't match the suit
  if (card.suit !== suit) {
    return false;
  }

  // Only Ace can be moved to an empty foundation pile
  if (foundationPile.length === 0) {
    if (card.rank === Rank.Ace) {
      return true;
    }
    return false;
  }

  const [lastFoundationPileCard] = foundationPile.slice(-1);

  // Destination and card moved should be consecutive
  if (convertRankToValue(lastFoundationPileCard.rank) - convertRankToValue(card.rank) === -1) {
    return true;
  }

  return false;
}
