import convertNumberToCard from './convertNumberToCard';

// Utility Functions

// Tests
describe('Joker is excluded', () => {
  test('when input is less than 0', () => {
    expect(() => convertNumberToCard(-1)).toThrowError(new Error('Input out of range (0-52)'));
  });

  test.each(Array.from({ length: 52 }, (_, index) => index))('when input = %i', (input: number) => {
    expect(convertNumberToCard(input)).toMatchSnapshot();
  });

  test('when input is greater or equal to 52', () => {
    expect(() => convertNumberToCard(52)).toThrowError(new Error('Input out of range (0-52)'));
  });
});

describe('Joker is included', () => {
  test('when input is less than 0', () => {
    expect(() => convertNumberToCard(-1, true)).toThrowError(new Error('Input out of range (0-54)'));
  });

  test.each(Array.from({ length: 54 }, (_, index) => index))('when input = %i', (input: number) => {
    expect(convertNumberToCard(input, true)).toMatchSnapshot();
  });

  test('when input is greater or equal to 54', () => {
    expect(() => convertNumberToCard(54, true)).toThrowError(new Error('Input out of range (0-54)'));
  });
});
