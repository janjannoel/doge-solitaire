import generateShuffledDeck from './generateShuffledDeck';

// Utility Functions

// Tests
test('should generate 52 cards', () => {
  const cards = generateShuffledDeck();
  expect(cards).toHaveLength(52);
});
