import { Card } from 'models/Card';
import { Suit } from 'models/Suit';
import { Rank } from 'models/Rank';
import { Color } from 'models/Color';

export default function mapNumberToCard(input: number, includeJoker = false): Card {
  const maxValue = includeJoker ? 54 : 52;

  if (input < 0 || input >= maxValue) {
    throw new Error(`Input out of range (0-${maxValue})`);
  }

  if (input === 52) {
    return {
      id: input,
      suit: Suit.Joker,
      rank: Rank.Joker,
      color: Color.Black,
      isFacedDown: true
    };
  }

  if (input === 53) {
    return {
      id: input,
      suit: Suit.Joker,
      rank: Rank.Joker,
      color: Color.Red,
      isFacedDown: true
    };
  }

  let suit: Suit;
  if (input < 13) {
    suit = Suit.Club;
  } else if (input < 26) {
    suit = Suit.Spade;
  } else if (input < 39) {
    suit = Suit.Heart;
  } else {
    suit = Suit.Diamond;
  }

  let rank: Rank;
  switch (input % 13) {
    case 12:
      rank = Rank.King;
      break;
    case 11:
      rank = Rank.Queen;
      break;
    case 10:
      rank = Rank.Jack;
      break;
    case 9:
      rank = Rank.Ten;
      break;
    case 8:
      rank = Rank.Nine;
      break;
    case 7:
      rank = Rank.Eight;
      break;
    case 6:
      rank = Rank.Seven;
      break;
    case 5:
      rank = Rank.Six;
      break;
    case 4:
      rank = Rank.Five;
      break;
    case 3:
      rank = Rank.Four;
      break;
    case 2:
      rank = Rank.Three;
      break;
    case 1:
      rank = Rank.Two;
      break;
    case 0:
    default:
      rank = Rank.Ace;
      break;
  }

  const color = input < 26 ? Color.Black : Color.Red;

  return {
    id: input,
    suit,
    rank,
    color,
    isFacedDown: true
  } as Card;
}
