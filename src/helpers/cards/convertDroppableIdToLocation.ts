import { Location } from 'models/Location';
import { PileType } from 'models/PileType';
import { Suit } from 'models/Suit';

export default function convertDroppableIdToLocation(droppableId: string): Location {
  const [pileType, position, index] = droppableId.split('-');

  switch (pileType) {
    case PileType.Tableau:
      return {
        pileType,
        position,
        index: index ? Number(index) : null
      };

    case PileType.Foundation:
      return {
        pileType,
        position: position as Suit,
        index: null
      };

    case PileType.Talon:
    case PileType.Stock:
    default:
      return {
        // @ts-ignore
        pileType,
        position: null,
        index: null
      };
  }
}
