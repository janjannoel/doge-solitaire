import shuffle from 'lodash/shuffle';

import { Pile } from 'models/Pile';
import convertNumberToCard from 'helpers/cards/convertNumberToCard';

export default function generateShuffledDeck(): Pile {
  return shuffle(Array.from({ length: 52 }, (_, index) => convertNumberToCard(index)));
}
